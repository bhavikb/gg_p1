{
    'name': 'Sms Integration',
    'category': 'general',
    'author': 'keyconcepts',
    'version': '9.0',
    'description':
        """
        """,
    'depends': ['base'],
    'data': [
        'security/ir.model.access.csv',
        'views/sms_view.xml',
        'wizard/sms_wizard_view.xml',
    ],
    'images': [],
    'auto_install': True,
    'installable': True,
}

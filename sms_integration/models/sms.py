
from openerp import api, fields, models
import requests

class SMSIntegration(models.Model):
    _name = "ir.sms.integration"
    _description = "SMS Integratiom"

    name = fields.Char('name', required=True)
    active = fields.Boolean('Active')
    url = fields.Text('URL', required=True)
    auth_key = fields.Char('AUTH_KEY', required=True)
    senderId = fields.Char('senderId')
    routeId = fields.Integer('routeId', default=1, placeholder="1")
    smsContentType = fields.Char('smsContentType')

# http://dnd.suratsms.net/rest/services/sendSMS/sendGroupSms?AUTH_KEY=b05730a9d1f5e5dd7d1dbede1aa979d&message=hii&senderId=KEYSMS&routeId=1&mobileNos=9723271313&smsContentType=english
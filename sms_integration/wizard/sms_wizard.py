# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import api, fields, models, _
import requests

class SmsSendWizard(models.TransientModel):
    _name = 'sms.send.wizard'
    _description = 'Point of Sale Payment'

    @api.model
    def default_get(self, fields):
        phone =''
        context = self._context or {}
        res = super(SmsSendWizard, self).default_get(fields)
        for partner_id in self.env['res.partner'].browse(context.get('active_ids')):
            if partner_id.phone:
                phone += partner_id.phone+','
        res['mobileNos'] = phone
        return res

    mobileNos = fields.Text('mobileNos', required=True)
    message = fields.Text('message', required=True)

    @api.multi
    def action_send_sms(self):
        ir_sms = self.env['ir.sms.integration'].search([])[0]
        request_url = ir_sms.url+'?AUTH_KEY='+ir_sms.auth_key+'&senderId='+ir_sms.senderId+'&routeId='+str(ir_sms.routeId)+'&smsContentType='+ir_sms.smsContentType+'&message='+self.message+'&mobileNos='+self.mobileNos

        parse_url = requests.request('GET', request_url)
        if parse_url:
            print 'SMS Sent Successfuly!!!!!!!'
        return

    

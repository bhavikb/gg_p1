# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-Today Key Concepts IT Services LLP.
#    (<http://keyconcepts.co.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

from openerp import fields, models, api

class HrApplicant(models.Model):
	_inherit = "hr.applicant"

	name = fields.Char("Subject / Application Name", required=False)

class HrJob(models.Model):
	_inherit = 'hr.job'

	description = fields.Text(string="Remark")

class HrEmployee(models.Model):
	_inherit = 'hr.employee'

	doj = fields.Date("Date Of Joining", default=fields.Date.context_today)
	blood_group = fields.Many2one('blood.group', 'Blood Group')

class BloodGroup(models.Model):
	_name = 'blood.group'

	name = fields.Char('Blood Group')
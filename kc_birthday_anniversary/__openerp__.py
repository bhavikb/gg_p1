# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-Today Key Concepts IT Services LLP.
#    (<http://keyconcepts.co.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

{
    'name': 'Birthday & Anniversary Reminder',
    'version': '9.0',
    'author': 'Key Concepts IT Services LLP.',
    'website': 'http://keyconcepts.co.in',
    'category': 'KC ',
    'depends': [
        'crm', 'kc_project', 'sms_integration',
    ],
    'data': [
        'views/res_partner_view.xml',
        'views/crm_lead_view.xml',
        'views/res_company_view.xml',
        'views/crm_project_view.xml',
        'data/wish_cronjob.xml',
    ],
}

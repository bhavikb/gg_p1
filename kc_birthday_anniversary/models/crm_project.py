from openerp import models, fields, api

class CrmProject(models.Model):
    _inherit = "crm.project"

    birthday_wish = fields.Text('Birthday Wish')
    anniversary_wish = fields.Text('Anniversary Wish')
    inquiry_no = fields.Char('Inquiry No')

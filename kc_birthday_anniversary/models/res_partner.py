from openerp import models, fields, api


class Partner(models.Model):
    _inherit = "res.partner"

    dob = fields.Date('Date of Birth')
    marital_status = fields.Selection([('married', 'Married'), ('unmarried', 'Unmarried')], string="Marital Status", default="unmarried")
    date_marriage = fields.Date('Date of Marriage')

from openerp import models, fields, api

class ResCompany(models.Model):
    _inherit = "res.company"

    dob_note = fields.Text('Date of Birth Note')
    marriage_note = fields.Text('Date of Marriage Note')

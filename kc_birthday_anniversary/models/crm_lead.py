from openerp import models, fields, api, _
from datetime import datetime, timedelta, time
import requests
# import urllib
from urlparse import urlparse

class CrmLead(models.Model):
    _inherit = "crm.lead"

    dob = fields.Date(related="partner_id.dob", string='Date of Birth')
    marital_status = fields.Selection([('married', 'Married'), ('unmarried', 'Unmarried')], related="partner_id.marital_status", string="Marital Status", default="unmarried")
    date_marriage = fields.Date(related="partner_id.date_marriage", string='Date of Marriage')

    @api.model
    def send_customer_birthday_email(self):
        today = datetime.now()
        today_month_day = '%-' + today.strftime('%m') + '-' + today.strftime('%d')
        # sms
        sms_obj = self.env['ir.sms.integration'].search([('active', '=', True)])
        lead_ids = self.env['crm.lead'].search([('partner_id.dob', 'like', today_month_day)])
        company_id = self.env['res.company'].search([])
        if lead_ids:
            for lead in lead_ids:
                if lead.partner_id.mobile:
                    if lead.project_id.birthday_wish:
                        bithday_msg = 'Dear ' + str(lead.partner_id.name) + ', ' + str(lead.project_id.name) + ' '+ str(lead.project_id.birthday_wish) + '-' + lead.project_id.inquiry_no
                        string = ""
                        for x in bithday_msg:
                            if x == " ":
                                string += "%20"
                            else:
                                string += x
                    else:
                        bithday_msg = 'Dear ' + str(lead.partner_id.name) + ', ' + str(lead.project_id.name) + ' '+ str(lead.company_id.dob_note) + '-' + str(lead.project_id.inquiry_no)
                        string = ""
                        for x in bithday_msg:
                            if x == " ":
                                string += "%20"
                            else:
                                string += x
                    ir_sms = self.env['ir.sms.integration'].search([])
                    request_url = '%s?AUTH_KEY=%s&senderId=%s&routeId=%s&smsContentType=%s&message=%s&mobileNos=%s' % (ir_sms.url, ir_sms.auth_key, ir_sms.senderId, str(ir_sms.routeId), ir_sms.smsContentType, string, lead.mobile2)
                    parse_url = requests.request('GET', request_url)
                    if parse_url:
                        print 'SMS Sent Successfuly!!!!!!!'

    @api.model
    def send_customer_anniversary_email(self):
        today = datetime.now()
        today_month_day = '%-' + today.strftime('%m') + '-' + today.strftime('%d')
        # sms
        sms_obj = self.env['ir.sms.integration'].search([('active', '=', True)])
        lead_ids = self.env['crm.lead'].search([('partner_id.date_marriage', 'like', today_month_day)])
        company_id = self.env['res.company'].search([])
        if lead_ids:
            for lead in lead_ids:
                if lead.partner_id.mobile:
                    if lead.project_id.birthday_wish:
                        bithday_msg = 'Dear ' + str(lead.partner_id.name) + ', ' + str(lead.project_id.name) + ' '+ str(lead.project_id.anniversary_wish) + '-' + lead.project_id.inquiry_no
                        string = ""
                        for x in bithday_msg:
                            if x == " ":
                                string += "%20"
                            else:
                                string += x
                    else:
                        bithday_msg = 'Dear ' + str(lead.partner_id.name) + ', ' + str(lead.project_id.name) + ' '+ str(lead.company_id.marriage_note) + '-' + lead.project_id.inquiry_no
                        string = ""
                        for x in bithday_msg:
                            if x == " ":
                                string += "%20"
                            else:
                                string += x
                    ir_sms = self.env['ir.sms.integration'].search([])
                    request_url = '%s?AUTH_KEY=%s&senderId=%s&routeId=%s&smsContentType=%s&message=%s&mobileNos=%s' % (ir_sms.url, ir_sms.auth_key, ir_sms.senderId, str(ir_sms.routeId), ir_sms.smsContentType, string, lead.mobile2)
                    parse_url = requests.request('GET', request_url)
                    if parse_url:
                        print 'SMS Sent Successfuly!!!!!!!'

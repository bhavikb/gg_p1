# -*- encoding: utf-8 -*-
###########################################################################
#    Module Writen to OpenERP, Open Source Management Solution
#
#    Copyright (c) 2015 Medma - http://keyconcepts.co.in
#    All Rights Reserved.
#
##############################################################################

{
    'name': 'Crm Lead SMS',
    'version': '1.0',
    'category': 'Kc',
    'author': 'Key Concepts IT Services LLP.',
    'website': 'http://keyconcepts.co.in',
    'depends': ['crm', 'sms_integration', 'kc_birthday_anniversary'],
    'data': [
        'views/crm_stage_view.xml',
    ],
    'test': [
    ],
    'demo': [
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}

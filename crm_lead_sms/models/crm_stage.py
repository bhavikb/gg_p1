from openerp import api, fields, models, _
import requests

class CrmProject(models.Model):
    _inherit = "crm.project"

    thanku_sms = fields.Text('Thank You SMS')


class crm_lead(models.Model):
    _inherit = "crm.lead" 

    @api.model
    def create(self, vals):
        res = super(crm_lead, self).create(vals)
        string = res.project_id.thanku_sms + '-' + res.project_id.inquiry_no
        if res.project_id.thanku_sms and res.mobile2:
            ir_sms = self.env['ir.sms.integration'].search([])[0]
            request_url = ir_sms.url+'?AUTH_KEY='+ir_sms.auth_key+'&senderId='+ir_sms.senderId+'&routeId='+str(ir_sms.routeId)+'&smsContentType='+ir_sms.smsContentType+'&message='+string+'&mobileNos='+res.mobile2
            parse_url = requests.request('GET', request_url)
            if parse_url:
                print 'SMS Sent Successfuly!!!!!!!'
        return res

from openerp import models, fields, api, _
from datetime import datetime, timedelta, time
import datetime

class CrmLead(models.Model):
    _inherit = "crm.lead"

    @api.model
    def send_next_actions_lead_reminder_mail(self):
        next_activities_ids = self.env['crm.lead'].search([('date_action', '=', datetime.date.today())])
        lead_ids = self.env['crm.lead'].read_group([('date_action', '=', datetime.date.today())], ['id', 'name', 'partner_id', 'project_id', 'phone', 'mobile2', 'user_id'], ['user_id'])
        for lead in lead_ids:
            table = ''
            email_to = ''
            user_id = self.env['res.users'].search([('id','=', lead['user_id'][0])])
            if user_id:
                email_to = user_id.login

            leads = ''
            for activities in next_activities_ids:
                if activities.user_id == user_id:
                    if activities.mobile2 and activities.phone:
                        mobile_phone = activities.mobile2 + '<br/>' + activities.phone
                    elif activities.mobile2 and not activities.phone:
                        mobile_phone = activities.mobile2
                    elif activities.phone and not activities.mobile2:
                        mobile_phone = activities.phone
                    elif not activities.phone and not activities.mobile2:
                        mobile_phone = ''
                    leads += "<tr><td><font face='Times New Roman' size='2.5'>%s</font></td><td><pre style='margin-top:0em; margin-bottom:0em;'><font face='Times New Roman' size='2.5'>%s</font></pre></td><td align='center'><pre style='margin-top:0em; margin-bottom:0em;'><font face='Times New Roman' size='2.5'>%s</font></pre></td><td><pre style='margin-top:0em; margin-bottom:0em;'><font face='Times New Roman' size='2.5'>%s</font></pre></td><td><pre style='margin-top:0em; margin-bottom:0em;'><font face='Times New Roman' size='2.5'>%s</font></pre></td><td><font face='Times New Roman' size='2.5'>%s</font></td></tr>" % (activities.name or '', activities.partner_id and activities.partner_id.name or '', mobile_phone or '', activities.email_from or '', activities.next_activity_id and activities.next_activity_id.name or '', activities.description or '')
            if leads:
                table = '<br/><br/><table border="1" style="border-collapse: collapse;" ><tr><th ><font face="Times New Roman" size="3">Opportunity</font></th><th ><font face="Times New Roman" size="3">Customer</font></th><th ><font face="Times New Roman" size="3">Mobile/Phone</font></th><th ><font face="Times New Roman" size="3">Email</font></th><th ><font face="Times New Roman" size="3">Activity</font></th><th><font face="Times New Roman" size="3">Notes</font></th>%s</table>' % (leads)

            body = '<b>Next Activities(Leads) %s</b>' % (datetime.date.today().strftime('%d/%m/%Y')) + table

            vals = {
                'model': None,
                'res_id': None,
                'parent_id': None,
                'subject': 'Next Activities(Lead) Reminder %s : %s ' % (datetime.date.today().strftime('%d/%m/%Y'), str(user_id.name)),
                'email_from': user_id.login,
                # 'email_cc': user_id.company_id.email,
                'email_to': email_to ,
                'body_html': body,
                'body': body,
                'auto_delete': True,
            }
            mail_mail_obj = self.env['mail.mail'].create(vals).send()
        return True

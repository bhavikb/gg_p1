odoo.define('kc_remove_export_option.RemoveExportOption', function (require){
"use strict";

var Model = require('web.DataModel');
var _t = require('web.core')._t;
var SUPERUSER_ID = 1;
var Sidebar = require('web.Sidebar');

Sidebar.include({
    add_items: function(section_code, items) {
        var self = this;
        var _super = this._super.bind(this);
        // allow Export for admin user
        var Users = new Model('res.users');
        Users.call('has_group', ['kc_remove_export_option.group_hide_export_kc']).done(function(is_export) {
            if (! is_export) {
                var export_label = _t("Export");
                if (section_code == 'other') {
                    for (var i = 0; i < items.length; i++) {
                        if (items[i]['label'] == export_label) {
                            items.splice(i, 1)
                        }
                    }
                }
            }
            if (items.length > 0) {
                _super(section_code, items);
            }
        });
    },

});

});

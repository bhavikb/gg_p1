from openerp import models, fields, api, _
from openerp.exceptions import UserError

class CrmLead(models.Model):
    _inherit = "crm.lead"
    _rec_name = "partner_id"

    broker_mobile = fields.Char(related="broker_id.mobile", string='Broker Mobile')
    flat_no = fields.Char(string="Flat No.")
    is_email_required = fields.Boolean("Is Email Required", default=False)

    def on_change_partner_id(self, cr, uid, ids, partner_id, context=None):
        values = {}
        if partner_id:
            partner = self.pool.get('res.partner').browse(cr, uid, partner_id, context=context)
            partner_name = (partner.parent_id and partner.parent_id.name) or (partner.is_company and partner.name) or False
            values = {
                'partner_name': partner_name,
                'contact_name': (not partner.is_company and partner.name) or False,
                'title': partner.title and partner.title.id or False,
                'street': partner.street,
                'street2': partner.street2,
                'city': partner.city,
                'state_id': partner.state_id and partner.state_id.id or False,
                'country_id': partner.country_id and partner.country_id.id or False,
                'email_from': partner.email,
                'phone': partner.phone,
                'mobile2': partner.mobile,
                'fax': partner.fax,
                'zip': partner.zip,
                'function': partner.function,
                'broker_id': partner.broker_name.id,
                'source_id': partner.source_id.id,
            }
        return {'value': values}


    @api.onchange('broker_mobile')
    def onchange_broker_name(self):
        if self.broker_mobile and not self.broker_id:
            broker = self.env['res.partner'].search([('mobile', '=', self.broker_mobile)])
            if broker:
                self.broker_id = broker
            else:
                broker = self.env['res.partner'].search([('phone', '=', self.broker_mobile)])
                self.broker_id = broker
                self.broker_mobile = broker.phone

    @api.model
    def create(self, vals):
        if vals['partner_id']:
            partner = self.env['res.partner'].search([('id', '=', vals['partner_id'])])
            if vals['mobile2']:
                partner.mobile = vals['mobile2']
                partner.lead = True
                partner.customer = False
            if vals['phone']:
                partner.phone = vals['phone']
                partner.lead = True
                partner.customer = False
            if vals['email_from']:
                partner.email = vals['email_from']
                partner.lead = True
                partner.customer = False
        if vals['broker_id']:
            partner = self.env['res.partner'].search([('id', '=', vals['broker_id'])])
            if vals['broker_mobile']:
                partner.mobile = vals['broker_mobile']
                partner.broker = True
                partner.customer = False
        return super(CrmLead, self).create(vals)

    @api.multi
    def write(self, vals):
        res = super(CrmLead, self).write(vals)
        if vals.get('mobile2'):
            partner = self.env['res.partner'].search([('id', '=', self.partner_id.id)])
            partner.mobile = vals.get('mobile2')
        if vals.get('phone'):
            partner = self.env['res.partner'].search([('id', '=', self.partner_id.id)])
            partner.phone = vals.get('phone')
        if vals.get('email_from'):
            partner = self.env['res.partner'].search([('id', '=', self.partner_id.id)])
            partner.email = vals.get('email_from')
        return res

    @api.onchange('mobile2')
    def onchange_mobile(self):
        self.is_email_required = True
        if not self.partner_id and self.mobile2:
            partner = self.env['res.partner'].search([('mobile', '=', self.mobile2)])
            if partner:
                self.partner_id = partner
                self.email_from = partner.email
                self.phone = partner.phone
            else:
                partner = self.env['res.partner'].search([('phone', '=', self.mobile2)])
                self.partner_id = partner
                self.email_from = partner.email
                self.mobile2 = partner.mobile

    @api.onchange('phone')
    def onchange_phone(self):
        if not self.partner_id and self.phone and not self.mobile:
            partner = self.env['res.partner'].search([('phone', '=', self.phone)])
            if partner:
                self.partner_id = partner
                self.email_from = partner.email
                self.mobile2 = partner.mobile
            else:
                partner = self.env['res.partner'].search([('mobile', '=', self.phone)])
                self.partner_id = partner
                self.email_from = partner.email
                self.phone = partner.phone

class LocationTag(models.Model):

    _name = "location.tag"
    _description = "Location Of Broker"

    name = fields.Char('Name', required=True)

    _sql_constraints = [
        ('name_uniq', 'unique (name)', "Tag name already exists !"),
    ]

class Partner(models.Model):
    _inherit = "res.partner"

    broker_location_ids = fields.Many2many("location.tag", string="Location" )
    lead_opportunity_count = fields.Integer("Opportunity", compute='_compute_lead_opportunity_count')
    broker_opportunity_count = fields.Integer("Broker Opportunity", compute='_compute_broker_opportunity_count')

    broker_name = fields.Many2one('res.partner', string="Broker name")
    broker_mobile = fields.Char(string="Broker Mobile")
    source_id = fields.Many2one('utm.source', string="Source")
    user_id = fields.Many2one('res.users', string='Salesperson',
      help='The internal user that is in charge of communicating with this contact if any.', default=lambda self: self.env.user)

    _sql_constraints = [
        ('phone_uniq', 'unique (phone)', "Phone already exists !"),
        ('mobile_uniq', 'unique (mobile)', "Mobile already exists !"),  ]

    @api.multi
    def _compute_lead_opportunity_count(self):
        for partner in self:
            operator = 'child_of' if partner.is_company else '='  # the opportunity count should counts the opportunities of this company and all its contacts
            partner.lead_opportunity_count = self.env['crm.lead'].search_count([('partner_id', operator, partner.id), ('type', '=', 'opportunity'), ('probability', '<', '100')])

    @api.multi
    def _compute_broker_opportunity_count(self):
        for partner in self:
            operator = 'child_of' if partner.is_company else '='  # the opportunity count should counts the opportunities of this company and all its contacts
            partner.broker_opportunity_count = self.env['crm.lead'].search_count([('broker_id', operator, partner.id), ('type', '=', 'opportunity')])

    @api.onchange('broker_name')
    def onchange_broker_mobile(self):
        if self.broker_name:
            self.broker_mobile = self.broker_name.mobile
